//const express = require("express");
import express from 'express';

//const http = require("http");
import http from 'http';
import { fileURLToPath } from 'url';
import json from 'body-parser';
import path from 'path';
import misRutas from './router/index.js'



const puerto = 80;
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


//declarar la variable punto de inicio


const app = express();

app.set("view engine","ejs");
app.set(express.static(__dirname + '/public'));
app.use(json.urlencoded({extendeds:true}));
app.use(misRutas.router);

app.use('/img',express.static(path.join(__dirname, 'public', 'img')));

app.listen(puerto,()=>{
    console.log(" Se inicio el servidor por el puerto: " + puerto);

})
